/*  Copyright (C) 2022 CZ.NIC, z.s.p.o. <knot-dns@labs.nic.cz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <fcntl.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include "lmdb_import1.h"

#define LOCKFILE "lock.mdb"
#define LOCKFILELEN 8
#define DATAFILE "data.mdb"
#define DATAFILELEN 8

extern void debug_datafile(void *map, size_t mapsize); // from datafile.c

static void debug_lockfile(void *map, size_t mapsize)
{
	MDB_txninfo *lockfile = map;
	MDB_reader *readers = lockfile->mti_readers;
	size_t nreaders = lockfile->mti_numreaders;

	printf("READER TABLE: magic %u format %u txnid %ld numreaders %u\n", lockfile->mti_magic, lockfile->mti_format, lockfile->mti_txnid, lockfile->mti_numreaders);

	for (size_t i = 0; i < nreaders; i++) {
		if (readers[i].mr_pid == 0 && readers[i].mr_tid == 0 && readers[i].mr_txnid == 0) {
			continue;
		}
		printf("READER[%zu] pid %u tid %lu txnid %ld\n", i, readers[i].mr_pid, readers[i].mr_tid, readers[i].mr_txnid);
	}
}

static void print_help(void)
{
	printf("TODO help\n");
}

static void concat_path(char *dest, const char *src1, size_t src1len,
                        const char *src2, size_t src2len)
{
	memcpy(dest, src1, src1len);
	dest[src1len] = '/';
	memcpy(dest + src1len + 1, src2, src2len);
	dest[src1len + 1 + src2len] = '\0';
}

static void *mmap_one(const char *filename, size_t filesize)
{
	int fd = open(filename, O_RDONLY);
	if (fd < 0) {
		perror("error opening file");
		return NULL;
	}

	void *map = mmap(NULL, filesize, PROT_READ, MAP_PRIVATE, fd, 0);
	close(fd);

	if (map == NULL || map == MAP_FAILED) {
		return NULL;
	}

	return map;
}

int main(int argc, char *argv[])
{
	if (argc != 2) {
		print_help();
		return 1;
	}

	const char *lmdbdir = argv[1];
	struct stat st = { 0 };

	if (stat(lmdbdir, &st) != 0 || !S_ISDIR(st.st_mode)) {
		printf("error: specified path is not a directory\n");
		print_help();
		return 1;
	}

	size_t lmdbdirlen = strlen(lmdbdir);
	char lockfile[lmdbdirlen + LOCKFILELEN + 2];
	char datafile[lmdbdirlen + DATAFILELEN + 2];
	size_t lockfilesize = 0, datafilesize = 0;
	void *lockf = NULL, *dataf = NULL;

	concat_path(lockfile, lmdbdir, lmdbdirlen, LOCKFILE, LOCKFILELEN);
	concat_path(datafile, lmdbdir, lmdbdirlen, DATAFILE, DATAFILELEN);

	if (stat(lockfile, &st) != 0 || !S_ISREG(st.st_mode) || (lockf = mmap_one(lockfile, st.st_size)) == NULL) {
                printf("error: lockfile can't be opened\n");
                print_help();
                return 1;
        }
	lockfilesize = st.st_size;

	if (stat(datafile, &st) != 0 || !S_ISREG(st.st_mode) || (dataf = mmap_one(datafile, st.st_size)) == NULL) {
                printf("error: datafile can't be openend\n");
                print_help();
		munmap(lockf, lockfilesize);
                return 1;
        }
	datafilesize = st.st_size;

	debug_lockfile(lockf, lockfilesize);

	//debug_datafile(dataf, datafilesize);

	munmap(lockf, lockfilesize);
	munmap(dataf, datafilesize);
	return 0;
}
