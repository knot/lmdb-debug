/*  Copyright (C) 2022 CZ.NIC, z.s.p.o. <knot-dns@labs.nic.cz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "lmdb_import2.h"

typedef struct {
	txnid_t first_freed;
} global_page_t;

global_page_t *global_pages = NULL;

size_t page_size; // global

#define PG(pages, id) ((pages) + (id) * page_size)
#define OPG(otherpage, id) (((void *)(otherpage)) + ((long)(id) - (long)(((MDB_page *)(otherpage))->mp_pgno)) * page_size)

static void print_indent(int indent)
{
	for (int i = 0; i < indent; i++) {
		printf("  ");
	}
}

static void print_page(MDB_page *mp, bool recursive, int indent, bool freedb)
{
	print_indent(indent);
	printf("page %08zu flags %04x keys %u left %hu ", mp->mp_pgno, mp->mp_flags, NUMKEYS(mp), SIZELEFT(mp));
	if (!recursive) {
		printf("first freed in txn %lu\n", global_pages[mp->mp_pgno].first_freed);
		print_indent(indent + 1);
		
		unsigned nkeys = NUMKEYS(mp);

		int i, low = IS_LEAF(mp) ? 0 : 1;
		int high = nkeys - 1;

		MDB_node *node;
		MDB_val nodekey;

		for (i = low; i <= high; i++) {
			node = NODEPTR(mp, i);
			nodekey.mv_size = NODEKSZ(node);
			nodekey.mv_data = NODEKEY(node);

			printf("\"");

			for (size_t j = 0; j < nodekey.mv_size; j++) {
				int c = *((uint8_t *)nodekey.mv_data + j);
				if (c < ' ' || c > 127 || c == '"') {
					printf("\\%03d", c);
				} else {
					printf("%c", c);
				}
			}

			printf("\" ");
		}
		printf("\n");
	} else if (IS_META(mp)) {
		MDB_meta *m = METADATA(mp);

		printf("magic %x version %u addr %p mapsize %zu last_pg %zu txnid %zu\n", m->mm_magic, m->mm_version, m->mm_address, m->mm_mapsize, m->mm_last_pg, m->mm_txnid);
		
		MDB_db *d = &m->mm_dbs[0];
		if ((long)d->md_root != -1) {
			print_indent(indent);
			printf("DB FREEDB pad %x fl %x depth %hu branches %zu leaves %zu overfl %zu entries %zu root %zu\n", d->md_pad, d->md_flags, d->md_depth, d->md_branch_pages, d->md_leaf_pages, d->md_overflow_pages, d->md_entries, d->md_root);
			print_indent(indent);
			printf("+++++++++++++++++++++++++++++++++++\n");
			print_page(OPG(mp, d->md_root), true, indent + 1, true);
		}
		
		d = &m->mm_dbs[1];
		print_indent(indent);
		printf("DB pad %x fl %x depth %hu branches %zu leaves %zu overfl %zu entries %zu root %zu\n", d->md_pad, d->md_flags, d->md_depth, d->md_branch_pages, d->md_leaf_pages, d->md_overflow_pages, d->md_entries, d->md_root);
		print_indent(indent);
		printf("+++++++++++++++++++++++++++++++++++\n");
		print_page(OPG(mp, d->md_root), true, indent + 1, false);

		printf("\n");
	} else if (!IS_BRANCH(mp) && freedb) {
		unsigned nkeys = NUMKEYS(mp);

		int i, low = IS_LEAF(mp) ? 0 : 1;
		int high = nkeys - 1;

		MDB_node *node;

		for (i = low; i <= high; i++) {
			node = NODEPTR(mp, i);

			txnid_t ti = *(uint64_t *)NODEKEY(node);

			printf(" (%ld)", ti);

			for (void *p = NODEDATA(node); p < NODEDATA(node) + NODEDSZ(node); p += sizeof(uint64_t)) {
				pgno_t pi = *(uint64_t *)p;
				printf(" %ld", pi);
				if (global_pages[pi].first_freed == 0 || global_pages[pi].first_freed > ti) {
					global_pages[pi].first_freed = ti;
				}
			}
		}
		printf("\n");
	} else if (IS_BRANCH(mp)) {
		unsigned nkeys = NUMKEYS(mp);

		int i, low = IS_LEAF(mp) ? 0 : 1;
		int high = nkeys - 1;
		(void)low;

		MDB_node *node;

		for (i = 0; i <= high; i++) {
			node = NODEPTR(mp, i);
			printf(" -> %zu", NODEPGNO(node));
		}
		printf("\n");

		if (recursive) {
			print_indent(indent);
			printf("============================================\n");
			for (i = 0; i <= high; i++) {
				node = NODEPTR(mp, i);
				print_page(OPG(mp, NODEPGNO(node)), true, indent + 1, freedb);
			}
		}
	}
}	

void debug_datafile(void *map, size_t mapsize)
{
	page_size = sysconf(_SC_PAGE_SIZE);
	
	global_pages = calloc(mapsize / page_size, sizeof(*global_pages));
	assert(global_pages != NULL);

	if (mapsize >= page_size) {
		print_page(map, true, 0, false);
	}
	if (mapsize >= page_size * 2) {
		print_page(map + page_size, true, 0, false);
	}
	
	const size_t start_page = 2;
	printf("+++++++++++++++++++++++++++++++++++\n\n");
	
	for (void *pi = map + start_page * page_size; pi + page_size <= map + mapsize; pi += page_size) {
		print_page(pi, false, 0, false);
	}
	
	free(global_pages);
}
