# LMDB-Debug

## About

LMDB-Debug is an utility to explore the internal structure of a LMDB database.

## Building

Dunno exactly the prerequisities, a mainstream Linux distro with build-essentials should be enough.

```
make
```

## Run

```
$ lmdb-debug /path/to/lmdb/directory
```

Don't point it to `data.mdb` or `lock.mdb` file instead, as it won't work.

## License

The original code of LMDB-Debug is licensed by GPLv3 or later.

The parts copy-pasted from LMDB itself (part of OpenLDAP Project) are licensed by OpenLDAP Public License.

Please see the beggining of each file for a note about license.
