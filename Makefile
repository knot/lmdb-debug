CFLAGS=-g

lmdb_debug: main.c datafile.o lmdb_import1.h lmdb.h midl.h
	$(CC) $(CFLAGS) -std=gnu99 -o $@ $< datafile.o

datafile.o: datafile.c lmdb_import2.h lmdb_import1.h lmdb.h midl.h
	$(CC) $(CFLAGS) -std=gnu99 -c -o $@ $<

clean:
	rm -f lmdb_debug datafile.o
